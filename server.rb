require 'webrick'

server = WEBrick::HTTPServer.new :Port => 5000

#The follwing proc is used to customize the server operations
server.mount_proc '/' do | request, responde|
  response.body = 'Hello, world!'
end

server.start
